package xyz.ejbca.wsdl;

import xyz.ejbca.wsdl.endpoints.CertificateAPI;
import xyz.ejbca.wsdl.endpoints.EndentityAPI;
import io.github.manusant.ss.SparkSwagger;
import io.github.manusant.ss.conf.Options;
import org.json.simple.parser.JSONParser;
import spark.Service;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

public class API {
    public static void main(String[] args) throws IOException {
        Config config = ConfigFactory.parseFile(new File(SparkSwagger.CONF_FILE_NAME));
        String url = config.getString("wsdl.url");
        String keypath = config.getString("wsdl.keypath");
        String keypass = config.getString("wsdl.keypass");
        Wsdl wsdl = new Wsdl(url, keypath, keypass);
        JSONParser parser = new JSONParser();
        Service spark = Service.ignite()
                .ipAddress(config.getString("spark-swagger.sparkhost"))
                .port(Integer.parseInt(config.getString("spark-swagger.sparkport")));
        SparkSwagger.of(spark, Options.defaultOptions().build()).endpoints(() -> Arrays.asList(new CertificateAPI(wsdl), new EndentityAPI(wsdl))).generateDoc();
    }
}
