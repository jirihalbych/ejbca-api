package xyz.ejbca.wsdl.endpoints;

import io.github.manusant.ss.route.Route;
import org.ejbca.core.protocol.ws.client.gen.UserMatch;
import org.json.simple.JSONObject;
import xyz.ejbca.wsdl.Wsdl;
import xyz.ejbca.wsdl.models.*;
import io.github.manusant.ss.SparkSwagger;
import io.github.manusant.ss.rest.Endpoint;
import io.github.manusant.ss.route.TypedRoute;
import spark.Request;
import spark.Response;

import static io.github.manusant.ss.descriptor.EndpointDescriptor.endpointPath;
import static io.github.manusant.ss.descriptor.MethodDescriptor.path;
import static io.github.manusant.ss.rest.RestResponse.ok;

public class EndentityAPI implements Endpoint {
    private static final String NAME_SPACE = "/endentity";
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(EndentityAPI.class);
    private Wsdl wsdl;
    public EndentityAPI(Wsdl wsdl){
        this.wsdl = wsdl;
    }

    @Override
    public void bind(final SparkSwagger restApi) {
        restApi.endpoint(endpointPath(NAME_SPACE)
                .withDescription("API pro endentity "), (q, a) -> log.info("Request pro Endentity API"))
                .post(path("/create")
                                .withDescription("Vytvori novou endentity, pokud uz existuje upravi data podle poslanych")
                                .withRequestType(EndentityRequest.class)
                                .withGenericResponse(), new TypedRoute<EndentityRequest>() {
                            @Override
                            public Object onRequest(EndentityRequest body, Request request, Response response) {
                                wsdl.addUser(body.getUsername(), body.getPassword(), body.getSubjectdn(), body.getSanalt(), body.getEmail(), body.getCaname(),
                                        body.getToken().toString(), body.getEprofile(), body.getCprofile());
                                return ok(response);
                            }
                        })
                .post(path("/reset")
                                .withRequestType(Endentity.class)
                                .withDescription("Resetuje endentity status zpatky na NEW")
                                .withGenericResponse(), new TypedRoute<Endentity>() {
                            @Override
                            public Object onRequest(Endentity user, Request request, Response response) {
                                wsdl.resetUser(user.getUsername(), user.getPassword());
                                return ok(response);
                            }
                        }
                )
                .get(path("/all")
                        .withDescription("Vrati vsechny endentity")
                        .withResponseAsMap(EndentityProfile.class), new Route() {
                    @Override
                    public Object onRequest(Request request, Response response) {
                        UserMatch usermatch = new UserMatch();
                        usermatch.setMatchwith(UserMatch.MATCH_WITH_DN);
                        usermatch.setMatchtype(UserMatch.MATCH_TYPE_CONTAINS);
                        usermatch.setMatchvalue("CN");
                        JSONObject data = wsdl.getUserWithFilter(usermatch);
                        return ok(response,data);}}
                )
                .get(path("/withfilter")
                    .withDescription("Vrati endentity podle filtru")
                        .withQueryParam().withRequired(true).withName("with").withDescription("Podle ceho:\n" +
                                "MATCH_WITH_USERNAME = 0\n" +
                                "MATCH_WITH_EMAIL = 1\n" +
                                "MATCH_WITH_STATUS = 2\n" +
                                "MATCH_WITH_ENDENTITYPROFILE = 3\n" +
                                "MATCH_WITH_CERTIFICATEPROFILE = 4\n" +
                                "MATCH_WITH_CA = 5\n" +
                                "MATCH_WITH_TOKEN = 6\n" +
                                "MATCH_WITH_DN = 7\n" +
                                "MATCH_WITH_UID = 100\n" +
                                "MATCH_WITH_COMMONNAME = 101\n" +
                                "MATCH_WITH_DNSERIALNUMBER = 102\n" +
                                "MATCH_WITH_GIVENNAME = 103\n" +
                                "MATCH_WITH_INITIALS = 104\n" +
                                "MATCH_WITH_SURNAME = 105\n" +
                                "MATCH_WITH_TITLE = 106\n" +
                                "MATCH_WITH_ORGANIZATIONALUNIT = 107\n" +
                                "MATCH_WITH_ORGANIZATION = 108\n" +
                                "MATCH_WITH_LOCALITY = 109\n" +
                                "MATCH_WITH_STATEORPROVINCE = 110\n" +
                                "MATCH_WITH_DOMAINCOMPONENT = 111\n" +
                                "MATCH_WITH_COUNTRY = 112\n").and()
                        .withQueryParam().withRequired(true).withName("type").withDescription("Tyo filtru:\n" +
                                "MATCH_TYPE_EQUALS = 0\n" +
                                "MATCH_TYPE_BEGINSWITH = 1\n" +
                                "MATCH_TYPE_CONTAINS = 2").and()
                        .withQueryParam().withRequired(true).withName("value").withDescription("Hodnota filtru").and()
                        .withResponseAsMap(EndentityProfile.class), new Route() {
                    @Override
                    public Object onRequest(Request request, Response response) {
                        UserMatch usermatch = new UserMatch();
                        usermatch.setMatchwith(Integer.parseInt(request.queryMap().get("with").value()));
                        usermatch.setMatchtype(Integer.parseInt(request.queryMap().get("type").value()));
                        usermatch.setMatchvalue(request.queryMap().get("value").value());
                        JSONObject data = wsdl.getUserWithFilter(usermatch);
                        return ok(response,data);
            }
        });
}
}
