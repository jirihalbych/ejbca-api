package xyz.ejbca.wsdl.endpoints;

import org.ejbca.core.protocol.ws.client.gen.UserMatch;
import xyz.ejbca.wsdl.Wsdl;
import xyz.ejbca.wsdl.models.*;
import io.github.manusant.ss.SparkSwagger;
import io.github.manusant.ss.descriptor.ParameterDescriptor;
import io.github.manusant.ss.rest.Endpoint;
import io.github.manusant.ss.route.Route;
import io.github.manusant.ss.route.TypedRoute;
import org.json.simple.JSONObject;
import spark.Request;
import spark.Response;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static io.github.manusant.ss.descriptor.EndpointDescriptor.endpointPath;
import static io.github.manusant.ss.descriptor.MethodDescriptor.path;
import static io.github.manusant.ss.rest.RestResponse.ok;

public class CertificateAPI implements Endpoint {
    private static final String NAME_SPACE = "/cert";
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(CertificateAPI.class);
    private Wsdl wsdl;
    public CertificateAPI(Wsdl wsdl){
        this.wsdl = wsdl;
    }

    @Override
    public void bind(final SparkSwagger restApi) {
        restApi.endpoint(endpointPath(NAME_SPACE)
                        .withDescription("API pro certifikaty "), (q, a) -> log.info("Request pro Cert API"))

                .get(path("/expiring")
                        .withDescription("Vrati expirujici certifikaty")
                        .withQueryParam().withRequired(true).withName("days").withDescription("Pocet dnu za jak dlouho vyprsi certifikaty").and()
                        .withQueryParam().withRequired(true).withName("count").withDescription("Pocet zobrazenych certifikatu").and()
                        .withResponseAsMap(Certificate.class), new Route() {
                    @Override
                    public Object onRequest(Request request, Response response) {
                        JSONObject data = wsdl.getExpiringCerts(Integer.parseInt(request.queryMap().get("days").value()),Integer.parseInt(request.queryMap().get("count").value()));
                        return ok(response,data);
                    }
                })
                .get(path("/withfilter")
                        .withDescription("Vrati endentity podle filtru")
                        .withQueryParam().withRequired(true).withName("type").withDescription("Typ filtru:\n" +
                                "Podle Serial number = 0\n" +
                                "Podle jmena identity = 1\n" +
                                "Polde jmena issuera = 2").and()
                        .withQueryParam().withRequired(true).withName("value").withDescription("Hodnota filtru").and()
                        .withResponseAsMap(Certificate.class), new Route() {
                    @Override
                    public Object onRequest(Request request, Response response) {
                        JSONObject data = wsdl.getAllCertificatesWithFilter(Integer.parseInt(request.queryMap().get("type").value()), request.queryMap().get("value").value());
                        return ok(response,data);
                    }
                })
            .post(path("/generate/cert")
                            .withDescription("Generuje certifikat z CSR")
                            .withRequestType(CertificateRequest.class)
                            .withGenericResponse(), new TypedRoute<CertificateRequest>() {
                        @Override
                        public Object onRequest(CertificateRequest body, Request request, Response response) {
                            wsdl.generateUserCertificate(body.getUsername(),body.getPassword(),body.getCsr(), body.getType().ordinal());
                            byte[] bytes = new byte[0];
                            try {
                                bytes = Files.readAllBytes(Paths.get(body.getUsername() + ".cert"));
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            HttpServletResponse raw = response.raw();
                            try {
                                raw.getOutputStream().write(bytes);
                                raw.getOutputStream().flush();
                                raw.getOutputStream().close();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            return ok(response);
                        }
                    })
                .delete(path("/revoke/:username")
                                .withDescription("Revokne certifikat endentity")
                                .withPathParam(ParameterDescriptor.newBuilder().withName("username").withDescription("Username endentity, ktere smazat cert").build())
                                .withGenericResponse(), new Route() {
                            @Override
                            public Object onRequest(Request request, Response response) {
                                wsdl.revokeUserCert(request.params(":username"));
                                return ok(response);
                            }
                })
                .post(path("/generate/keystore")
                                .withDescription("Generuje Keystore (P12)")
                                .withRequestType(KeystoreRequest.class)
                                .withGenericResponse(), new TypedRoute<KeystoreRequest>() {
                    @Override
                    public Object onRequest(KeystoreRequest body, Request request, Response response) {
                        wsdl.generateUserCertificates(body.getUsername(), body.getPassword(), body.getAlg(), body.getSpec());
                        byte[] bytes = new byte[0];
                        try {
                            bytes = Files.readAllBytes(Paths.get(body.getUsername() + ".p12"));
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        HttpServletResponse raw = response.raw();
                        try {
                            raw.getOutputStream().write(bytes);
                            raw.getOutputStream().flush();
                            raw.getOutputStream().close();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        return ok(response);
                    }
                        });
    }
}
