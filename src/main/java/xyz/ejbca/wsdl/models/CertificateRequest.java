package xyz.ejbca.wsdl.models;

public class CertificateRequest {
    public enum Certtype {
        PKCS10,
        CRMF,
        SPKAC,
        PUBLICKEY;
    }
    String username;
    String password;
    String csr;
    Certtype type;
    public CertificateRequest(String username, String password, String csr, Certtype type){
        this.username = username;
        this.password = password;
        this.type = type;
        this.csr = csr;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getCsr() {
        return csr;
    }

    public Certtype getType() {
        return type;
    }
}
