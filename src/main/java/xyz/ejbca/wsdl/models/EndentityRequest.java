package xyz.ejbca.wsdl.models;

public class EndentityRequest {
    public enum Token{
        USERGENERATED,
        P12,
        JKS,
        PEM,
        BCFKS
    }
    String username;
    String password;
    String subjectdn;
    String sanalt;
    String email = null;
    String caname;
    Token token;
    String eprofile;
    String cprofile;

    public EndentityRequest(String username, String password, String subjectdn, String sanalt, String email, String caname, Token token, String eprofile, String cprofile) {
        this.username = username;
        this.password = password;
        this.subjectdn = subjectdn;
        this.sanalt = sanalt;
        this.email = email;
        this.caname = caname;
        this.token = token;
        this.eprofile = eprofile;
        this.cprofile = cprofile;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getSubjectdn() {
        return subjectdn;
    }

    public String getSanalt() {
        return sanalt;
    }


    public String getEmail() {
        return email;
    }

    public String getCaname() {
        return caname;
    }

    public Token getToken() {
        return token;
    }

    public String getEprofile() {
        return eprofile;
    }

    public String getCprofile() {
        return cprofile;
    }
}
