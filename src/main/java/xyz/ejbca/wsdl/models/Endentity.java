package xyz.ejbca.wsdl.models;

public class Endentity {
    String username;
    String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Endentity(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
