package xyz.ejbca.wsdl.models;

public class EndentityProfile {
    String username;
    String subjectDN;
    String caName;
    String subjectAltName;
    String email;
    int Status;
    String tokenType;
    String endentityProfile;
    String certificateProfile;
    String startTime;
    String endTime;
    String extendedInformation;
}
