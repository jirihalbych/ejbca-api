package xyz.ejbca.wsdl.models;

public class KeystoreRequest {
    String username;
    String password;
    String spec;
    String alg;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getSpec() {
        return spec;
    }

    public String getAlg() {
        return alg;
    }

    public KeystoreRequest(String username, String password, String spec, String alg) {
        this.username = username;
        this.password = password;
        this.spec = spec;
        this.alg = alg;
    }
}
