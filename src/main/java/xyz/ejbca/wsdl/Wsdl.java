package xyz.ejbca.wsdl;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import io.github.manusant.ss.SparkSwagger;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.cesecore.util.CryptoProviderTools;
import org.ejbca.core.protocol.ws.client.gen.*;
import org.ejbca.core.protocol.ws.common.CertificateHelper;
import org.json.simple.JSONObject;

import javax.xml.namespace.QName;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;

public class Wsdl {
    private EjbcaWS ejbcaraws;

    public Wsdl(String url, String keypath, String keypass) throws MalformedURLException {
        CryptoProviderTools.installBCProvider();
        System.setProperty("javax.net.ssl.keyStore", keypath);
        System.setProperty("javax.net.ssl.keyStorePassword", keypass);
        //System.setProperty("javax.net.ssl.trustStore","p12/wstest.jks");
        //System.setProperty("javax.net.ssl.trustStorePassword","foo123");
        QName qname = new QName("http://ws.protocol.core.ejbca.org/", "EjbcaWSService");
        EjbcaWSService service = new EjbcaWSService(new URL(url), qname);
        this.ejbcaraws = service.getEjbcaWSPort();
    }
    UserDataVOWS getUser(String username) {
        UserMatch usermatch = new UserMatch();
        usermatch.setMatchwith(UserMatch.MATCH_WITH_USERNAME);
        usermatch.setMatchtype(UserMatch.MATCH_TYPE_EQUALS);
        usermatch.setMatchvalue(username);
        List<UserDataVOWS> result = null;
        try {
            result = ejbcaraws.findUser(usermatch);
        } catch (AuthorizationDeniedException_Exception e) {
            throw new RuntimeException(e);
        } catch (EjbcaException_Exception e) {
            throw new RuntimeException(e);
        } catch (EndEntityProfileNotFoundException_Exception e) {
            throw new RuntimeException(e);
        } catch (IllegalQueryException_Exception e) {
            throw new RuntimeException(e);
        }
        return result.get(0);
    }
    public void resetUser(String username, String password) {
        UserDataVOWS user = getUser(username);
        user.setPassword(password);
        user.setStatus(10);
        try {
            ejbcaraws.editUser(user);
        } catch (ApprovalException_Exception e) {
            throw new RuntimeException(e);
        } catch (AuthorizationDeniedException_Exception e) {
            throw new RuntimeException(e);
        } catch (CADoesntExistsException_Exception e) {
            throw new RuntimeException(e);
        } catch (EjbcaException_Exception e) {
            throw new RuntimeException(e);
        } catch (UserDoesntFullfillEndEntityProfile_Exception e) {
            throw new RuntimeException(e);
        } catch (WaitingForApprovalException_Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void generateUserCertificates(String username, String password, String alg, String spec) {
        UserDataVOWS user = getUser(username);
        user.setPassword(password);
        KeyStore keystore = null;
        try {
            keystore = ejbcaraws.softTokenRequest(user, null, spec, alg);
        } catch (ApprovalException_Exception e) {
            throw new RuntimeException(e);
        } catch (AuthorizationDeniedException_Exception e) {
            throw new RuntimeException(e);
        } catch (CADoesntExistsException_Exception e) {
            throw new RuntimeException(e);
        } catch (EjbcaException_Exception e) {
            throw new RuntimeException(e);
        } catch (NotFoundException_Exception e) {
            throw new RuntimeException(e);
        } catch (UserDoesntFullfillEndEntityProfile_Exception e) {
            throw new RuntimeException(e);
        } catch (WaitingForApprovalException_Exception e) {
            throw new RuntimeException(e);
        }
        java.security.KeyStore pkcs12 = null;
        try {
            pkcs12 = java.security.KeyStore.getInstance("PKCS12");
        } catch (KeyStoreException e) {
            throw new RuntimeException(e);
        }
        byte[] data = keystore.getRawKeystoreData();
        InputStream in = new ByteArrayInputStream(data);
        try {
            pkcs12.load(in,password.toCharArray());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (CertificateException e) {
            throw new RuntimeException(e);
        }
        java.security.cert.Certificate[] certs = new java.security.cert.Certificate[0];
        try {
            certs = pkcs12.getCertificateChain(username);
        } catch (KeyStoreException e) {
            throw new RuntimeException(e);
        }
        final StringWriter writer = new StringWriter();
        final JcaPEMWriter pemWriter = new JcaPEMWriter(writer);
        for (java.security.cert.Certificate cert : certs){
            try {
                pemWriter.writeObject(cert);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        try {
            pemWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            pemWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            FileUtils.write(new File(username + ".pem"),writer.toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            FileUtils.writeByteArrayToFile(new File(username + ".p12"),data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void generateUserCertificate(String username, String password, String csr, int type) {
        UserDataVOWS user = getUser(username);
        user.setPassword(password);
        CertificateResponse cert = null;
        try {
            cert = ejbcaraws.certificateRequest(user, csr, type, null, CertificateHelper.RESPONSETYPE_CERTIFICATE);
        } catch (ApprovalException_Exception e) {
            throw new RuntimeException(e);
        } catch (AuthorizationDeniedException_Exception e) {
            throw new RuntimeException(e);
        } catch (EjbcaException_Exception e) {
            throw new RuntimeException(e);
        } catch (NotFoundException_Exception e) {
            throw new RuntimeException(e);
        } catch (UserDoesntFullfillEndEntityProfile_Exception e) {
            throw new RuntimeException(e);
        } catch (WaitingForApprovalException_Exception e) {
            throw new RuntimeException(e);
        }
        try {
            FileUtils.writeByteArrayToFile(new File(username + ".cert"),cert.getRawData());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void revokeUserCert(String username) {
        try {
            ejbcaraws.revokeUser(username, 1, false);
        } catch (AlreadyRevokedException_Exception e) {
            throw new RuntimeException(e);
        } catch (ApprovalException_Exception e) {
            throw new RuntimeException(e);
        } catch (AuthorizationDeniedException_Exception e) {
            throw new RuntimeException(e);
        } catch (CADoesntExistsException_Exception e) {
            throw new RuntimeException(e);
        } catch (EjbcaException_Exception e) {
            throw new RuntimeException(e);
        } catch (NotFoundException_Exception e) {
            throw new RuntimeException(e);
        } catch (WaitingForApprovalException_Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void addUser(String username, String password, String subjectdn, String sanalt, String email, String caname, String token, String eprofilename, String cprofilename) {
        UserDataVOWS user = new UserDataVOWS();
        user.setUsername(username);
        user.setPassword(password);
        user.setSubjectDN(subjectdn);
        user.setSubjectAltName(sanalt);
        user.setTokenType(token);
        user.setStatus(10);
        user.setEmail(email);
        user.setCaName(caname);
        user.setEndEntityProfileName(eprofilename);
        user.setCertificateProfileName(cprofilename);
        try {
            ejbcaraws.editUser(user);
        } catch (ApprovalException_Exception e) {
            throw new RuntimeException(e);
        } catch (AuthorizationDeniedException_Exception e) {
            throw new RuntimeException(e);
        } catch (CADoesntExistsException_Exception e) {
            throw new RuntimeException(e);
        } catch (EjbcaException_Exception e) {
            System.out.println(e.getFaultInfo().getMessage());
            throw new RuntimeException(e);
        } catch (UserDoesntFullfillEndEntityProfile_Exception e) {
            throw new RuntimeException(e);
        } catch (WaitingForApprovalException_Exception e) {
            throw new RuntimeException(e);
        }
    }

    public JSONObject getExpiringCerts(int days, int count) {
        List<Certificate> certs = null;
        try {
            certs = ejbcaraws.getCertificatesByExpirationTime(days, count);
        } catch (EjbcaException_Exception e) {
            throw new RuntimeException(e);
        }
        return certificatesToJson(certs, -1, "");
    }

    public JSONObject getUserWithFilter(UserMatch usermatch) {
        List<UserDataVOWS> result = null;
        try {
            result = ejbcaraws.findUser(usermatch);
        } catch (AuthorizationDeniedException_Exception e) {
            throw new RuntimeException(e);
        } catch (EjbcaException_Exception e) {
            throw new RuntimeException(e);
        } catch (EndEntityProfileNotFoundException_Exception e) {
            throw new RuntimeException(e);
        } catch (IllegalQueryException_Exception e) {
            throw new RuntimeException(e);
        }
        JSONObject finaljson = new JSONObject();
        int index = 0;
        for (UserDataVOWS userdata : result){
            JSONObject json = new JSONObject();
            json.put("Username",userdata.getUsername());
            json.put("Subject DN",userdata.getSubjectDN());
            json.put("CA name",userdata.getCaName());
            json.put("Subject Alt Name",userdata.getSubjectAltName());
            json.put("Email",userdata.getEmail());
            json.put("Status",userdata.getStatus());
            json.put("Token type",userdata.getTokenType());
            json.put("Endentity profile",userdata.getEndEntityProfileName());
            json.put("Certificate profile",userdata.getCertificateProfileName());
            json.put("Start time",userdata.getStartTime());
            json.put("End time",userdata.getEndTime());
            json.put("Extended information",userdata.getExtendedInformation());
            finaljson.put(index++,json);
        }
        return finaljson;
    }

    public JSONObject getAllCertificatesWithFilter(int type, String value){
        List<Certificate> certificates = null;
        try {
            certificates = ejbcaraws.getCertificatesByExpirationTime(999999999, 99999);
        } catch (EjbcaException_Exception e) {
            throw new RuntimeException(e);
        }
        System.out.println(certificates);
        return certificatesToJson(certificates, type, value);
    }

    JSONObject certificatesToJson(List<Certificate> certificates, int type, String value){
        JSONObject finaljson = new JSONObject();
        int index = 0;
        for (Certificate cert : certificates){
            X509Certificate xcert = certToX509(cert);
            switch (type){
                case 0:
                    if(!String.valueOf(xcert.getSerialNumber()).equals(value))continue;
                    break;
                case 1:
                    if(!xcert.getSubjectX500Principal().getName().equals(value))continue;
                    break;
                case 2:
                    if(!xcert.getIssuerX500Principal().getName().equals(value))continue;
                    break;
            }
            JSONObject json = new JSONObject();
            json.put("Serial number", String.valueOf(xcert.getSerialNumber()));
            json.put("Name", xcert.getSubjectX500Principal().getName());
            json.put("Issuer", xcert.getIssuerX500Principal().getName());
            json.put("End time", xcert.getNotAfter());
            json.put("Start time", xcert.getNotBefore());
            finaljson.put(index++,json);
        }
        return finaljson;
    }

    X509Certificate certToX509(Certificate cert){
        CertificateFactory certFactory = null;
        try {
            certFactory = CertificateFactory.getInstance("X.509");
        } catch (CertificateException e) {
            throw new RuntimeException(e);
        }
        byte[] data = cert.getRawCertificateData();
        InputStream in = new ByteArrayInputStream(data);
        X509Certificate xcert = null;
        try {
            xcert = (X509Certificate)certFactory.generateCertificate(in);
        } catch (CertificateException e) {
            throw new RuntimeException(e);
        }
        return xcert;
    }

}
